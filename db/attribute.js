/* jshint indent: 4 */

module.exports = (sequelize, DataTypes) => sequelize.define('attribute', {
    id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    categoryId: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
            model: 'categories',
            key: 'id'
        },
        unique: true
    },
    type: {
        type: DataTypes.ENUM("ATTR_NUMBER", "ATTR_OPTION", "ATTR_LIST", "ATTR_ALTER"),
        allowNull: false
    },
    value: {
        type: DataTypes.JSONB,
        allowNull: false
    }
}, {
    tableName: 'attribute',
    timestamps: false
});
