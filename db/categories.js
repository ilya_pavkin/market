/* jshint indent: 4 */

module.exports = (sequelize, DataTypes) => sequelize.define('categories', {
    id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    parentId: {
        type: DataTypes.BIGINT,
        allowNull: true,
        references: {
            model: 'categories',
            key: 'id'
        }
    }
}, {
    tableName: 'categories',
    timestamps: false,
    indexes: [{
        name: 'root_category_unique',
        unique: true,
        method: 'BTREE',
        fields: ['name'],
        where: {
            parentId: null
        }
    }, {
        name: 'subcategory_unique',
        unique: true,
        method: 'BTREE',
        fields: ['name', 'parentId'],
        where: {
            parentId: {
                [sequelize.Op.ne]: null
            }
        }
    }]
});
