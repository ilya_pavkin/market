/* jshint indent: 4 */

module.exports = (sequelize, DataTypes) => sequelize.define('variant', {
    id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    attribs: {
        type: DataTypes.JSONB,
        allowNull: false
    },
    productId: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
            model: 'product',
            key: 'id'
        }
    }
}, {
    tableName: 'variant',
    timestamps: false
});
