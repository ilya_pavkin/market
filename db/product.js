/* jshint indent: 4 */

module.exports = (sequelize, DataTypes) => sequelize.define('product', {
    id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        unique: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    attribs: {
        type: DataTypes.JSONB,
        allowNull: false
    }
}, {
    tableName: 'product',
    timestamps: false
});
