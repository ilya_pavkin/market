--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: attributeType; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE "attributeType" AS ENUM (
    'ATTR_NUMBER',
    'ATTR_OPTION',
    'ATTR_LIST',
    'ATTR_ALTER'
);


ALTER TYPE "attributeType" OWNER TO postgres;

--
-- Name: enum_attribute_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE enum_attribute_type AS ENUM (
    'ATTR_NUMBER',
    'ATTR_OPTION',
    'ATTR_LIST',
    'ATTR_ALTER'
);


ALTER TYPE enum_attribute_type OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: attribute; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE attribute (
    id bigint NOT NULL,
    name character varying(256) NOT NULL,
    description text,
    "categoryId" bigint NOT NULL,
    type "attributeType" NOT NULL,
    value jsonb NOT NULL
);


ALTER TABLE attribute OWNER TO postgres;

--
-- Name: attribute_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE attribute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE attribute_id_seq OWNER TO postgres;

--
-- Name: attribute_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE attribute_id_seq OWNED BY attribute.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE categories (
    id bigint NOT NULL,
    name character varying(256) NOT NULL,
    description text,
    "parentId" bigint
);


ALTER TABLE categories OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categories_id_seq OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- Name: product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product (
    id bigint NOT NULL,
    name character varying NOT NULL,
    description text NOT NULL,
    attribs jsonb NOT NULL
);


ALTER TABLE product OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_id_seq OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_id_seq OWNED BY product.id;


--
-- Name: variant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE variant (
    "productId" bigint NOT NULL
)
INHERITS (product);


ALTER TABLE variant OWNER TO postgres;

--
-- Name: attribute id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY attribute ALTER COLUMN id SET DEFAULT nextval('attribute_id_seq'::regclass);


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- Name: product id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product ALTER COLUMN id SET DEFAULT nextval('product_id_seq'::regclass);


--
-- Name: variant id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY variant ALTER COLUMN id SET DEFAULT nextval('product_id_seq'::regclass);


--
-- Name: attribute attribute_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY attribute
    ADD CONSTRAINT attribute_pkey PRIMARY KEY (id);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: product product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: product product_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_unique UNIQUE (id, name);


--
-- Name: attribute unique_attribute; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY attribute
    ADD CONSTRAINT unique_attribute UNIQUE ("categoryId", name);


--
-- Name: variant variant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY variant
    ADD CONSTRAINT variant_pkey PRIMARY KEY (id);


--
-- Name: root_category_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX root_category_unique ON categories USING btree (name) WHERE ("parentId" IS NULL);


--
-- Name: subcategory_unique; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX subcategory_unique ON categories USING btree (name, "parentId") WHERE ("parentId" IS NOT NULL);


--
-- Name: attribute categoryFK; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY attribute
    ADD CONSTRAINT "categoryFK" FOREIGN KEY ("categoryId") REFERENCES categories(id);


--
-- Name: categories categoryParent; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT "categoryParent" FOREIGN KEY ("parentId") REFERENCES categories(id);


--
-- Name: variant variant_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY variant
    ADD CONSTRAINT variant_fkey FOREIGN KEY ("productId") REFERENCES product(id);


--
-- PostgreSQL database dump complete
--

