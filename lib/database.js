const env = process.env.NODE_ENV || 'development';
const path = require('path');
const fs = require('fs');
const Sequelize = require('sequelize');
const sequelizeLogger = require('sequelize-log-syntax-colors');

const config = require(path.join(__dirname, '..', 'config', 'dbconfig.json'))[env];

console.log(`Running sequelize in enviroment ${env} on database ${config.database}`);

if (env === 'test-silenced') {
    config.logging = null;
} else {
    config.logging = sequelizeLogger;
}

const sequelize = new Sequelize(config.database, config.username, config.password, config);
const db = {};

fs
    .readdirSync('db')
    .forEach((file) => {
        console.log(`Loading db accessor: ${file}`);
        const model = sequelize.import(`../${path.join('db', file)}`);
        db[model.name] = model;
    });

fs
    .readdirSync('entities')
    .forEach((file) => {
        console.log(`Loading entities: ${file}`);
        const modelName = path.basename(file, '.js');
        if (db[modelName] !== undefined) {
            const model = require(`../${path.join('entities', file)}`)(db[modelName]);
            if (model.instanceMethods !== undefined) {
                Object.keys(model.instanceMethods).forEach((methodName) => {
                    db[modelName].prototype[methodName] = model.instanceMethods[methodName];
                });
            }
            if (model.classMethods !== undefined) {
                Object.keys(model.classMethods).forEach((methodName) => {
                    db[modelName][methodName] = model.classMethods[methodName];
                });
            }
        }
    });

Object.keys(db).forEach((modelName) => {
    if ('associate' in db[modelName]) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
