const env = process.env.NODE_ENV || 'test';
process.env.NODE_ENV = env;

const Assert = require('assert');
const DBUtil = require('../util/database-test-asset.js');
const sqlz = require('../lib/database.js');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const chaiString = require('chai-string');

const silenced = (env === 'test-silenced');

chai.use(chaiAsPromised);
chai.use(chaiString);
chai.should();

function printErrCorrect(err) {
    if (!silenced) {
        console.log(`Catch correct error - ${err.name}: ${err.message}`);
    }
}

function duplicateErrorHandler(err) {
    err.should.be.an.instanceOf(Error);
    err.name.should.equal('SequelizeUniqueConstraintError');
    err.message.should.equal('Validation error');
    printErrCorrect(err);
    return err;
}

function nullValidationErrorHandler(err) {
    err.should.be.an.instanceOf(Error);
    err.name.should.equal('SequelizeValidationError');
    err.message.should.startWith('notNull Violation');
    printErrCorrect(err);
    return err;
}

function shouldNotBeSuccessful(object) {
    object.should.be.an.instanceOf(Error);
    return object;
}

describe('Data accessors tests', function () {
    this.timeout(0);
    before((done) => {
        DBUtil.dumpSchema('.tmp/schema', () => {
            DBUtil.dropDatabase(() => {
                DBUtil.createDatabase(() => {
                    DBUtil.restoreDatabase('.tmp/schema', done, 'test');
                }, 'test');
            }, 'test');
        }, 'development');
    });
    after((done) => {
        this.timeout(0);
        sqlz.sequelize.close();
        done();
    });
    describe('Categories', () => {
        beforeEach(() => sqlz.sequelize.sync({
            force: true
        }));
        afterEach(() => sqlz.categories.destroy({
            where: {}
        }));
        it('should create category', () => sqlz.categories.create({
            name: 'test'
        })
            .then(category => category.dataValues.id));
        it('should not create unnamed category', () => sqlz.categories.create({})
            .catch(nullValidationErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should not create category duplicate', () => sqlz.categories.create({
            name: 'test'
        })
            .then(() => sqlz.categories.create({
                name: 'test'
            }))
            .catch(duplicateErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should create subcategory', () => sqlz.categories.create({
            name: 'test'
        })
            .then(parent => sqlz.categories.create({
                name: 'test',
                parentId: parent.dataValues.id
            })));
        it('should not create subcategory duplicate', () => sqlz.categories.create({
            name: 'test'
        })
            .then(parent => sqlz.categories.create({
                name: 'test',
                parentId: parent.dataValues.id
            }))
            .then(result => sqlz.categories.create({
                name: 'test',
                parentId: result.dataValues.parentId
            }))
            .catch(duplicateErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should rename category', () => sqlz.categories.create({
            name: 'test'
        })
            .then(object => sqlz.categories.update({
                name: 'test2'
            }, {
                where: {
                    id: object.dataValues.id
                }
            })));
        it('should not rename category to exist name (create duplicate)', () => sqlz.categories.create({
            name: 'test'
        })
            .then(() => sqlz.categories.create({
                name: 'test2'
            }))
            .then(object => sqlz.categories.update({
                name: 'test'
            }, {
                where: {
                    id: object.dataValues.id
                }
            }))
            .catch(duplicateErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should rename subcategory', () => sqlz.categories.create({
            name: 'test'
        })
            .then(parent => sqlz.categories.create({
                name: 'test',
                parentId: parent.dataValues.id
            }))
            .then(object => sqlz.categories.update({
                name: 'test2'
            }, {
                where: {
                    id: object.dataValues.id
                }
            })));
        it('should not rename subcategory to exist name (create dublicate)', () => sqlz.categories.create({
            name: 'test'
        })
            .then(parent => sqlz.categories.create({
                name: 'test',
                parentId: parent.dataValues.id
            }))
            .then(object => sqlz.categories.create({
                name: 'test2',
                parentId: object.dataValues.parentId
            }))
            .then(object => sqlz.categories.update({
                name: 'test',
            }, {
                where: {
                    id: object.dataValues.id
                }
            }))
            .catch(duplicateErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should set category as subcategory (move to category)', () => {
            let src = null;
            return sqlz.categories.create({
                name: 'test'
            })
                .then((result) => {
                    src = result;
                    return result;
                })
                .then(() => sqlz.categories.create({
                    name: 'test2'
                }))
                .then(object => sqlz.categories.update({
                    parentId: src.dataValues.id
                }, {
                    where: {
                        id: object.dataValues.id
                    }
                }));
        });
        it('should set subcategory as category (move to root)', () => sqlz.categories.create({
            name: 'test'
        })
            .then(parent => sqlz.categories.create({
                name: 'test2',
                parentId: parent.dataValues.id
            }))
            .then(object => sqlz.categories.update({
                parentId: null
            }, {
                where: {
                    id: object.dataValues.id
                }
            })));
        it('should move subcategory to another category', () => {
            let src = null;
            return sqlz.categories.create({
                name: 'test'
            })
                .then(parent => sqlz.categories.create({
                    name: 'test',
                    parentId: parent.dataValues.id
                }))
                .then((result) => {
                    src = result;
                    return result;
                })
                .then(() => sqlz.categories.create({
                    name: 'test2'
                }))
                .then(object => sqlz.categories.update({
                    parentId: object.dataValues.id
                }, {
                    where: {
                        id: src.dataValues.id
                    }
                }));
        });
        it('should not move category to another category contains same-named subcategory', () => {
            let src = null;
            return sqlz.categories.create({
                name: 'test'
            })
                .then(parent => sqlz.categories.create({
                    name: 'test',
                    parentId: parent.dataValues.id
                }))
                .then((result) => {
                    src = result;
                    return result;
                })
                .then(() => sqlz.categories.create({
                    name: 'test2'
                }))
                .then(parent => sqlz.categories.create({
                    name: 'test',
                    parentId: parent.dataValues.id
                })
                    .then(() => parent))
                .then(object => sqlz.categories.update({
                    parentId: object.dataValues.id
                }, {
                    where: {
                        id: src.dataValues.id
                    }
                }))
                .catch(duplicateErrorHandler);
        });
        it('should change description of category', () => sqlz.categories.create({
            name: 'test'
        })
            .then(category => sqlz.categories.update({
                description: 'Some test stuff'
            }, {
                where: {
                    id: category.dataValues.id
                }
            })));
    });
    describe('Attributes', () => {
        let cat = null;
        beforeEach(() => sqlz.sequelize
            .sync({
                force: true
            })
            .then(() => sqlz.categories.create({
                name: 'test'
            }))
            .then((result) => {
                cat = result;
                return result;
            }));
        it('should create attribute', () => sqlz.attribute.create({
            name: 'testAttrib',
            type: 'ATTR_NUMBER',
            value: {},
            categoryId: cat.dataValues.id
        }));
        it('should not create unassigned attribute', () => sqlz.attribute.create({
            name: 'testAttrib',
            type: 'ATTR_LIST',
            value: {}
        })
            .catch(nullValidationErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should not create unnamed attribute', () => sqlz.attribute.create({
            type: 'ATTR_LIST',
            value: {},
            categoryId: cat.dataValues.id
        })
            .catch(nullValidationErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should not create attribute without type', () => sqlz.attribute.create({
            name: 'testAttrib',
            value: {},
            categoryId: cat.dataValues.id
        })
            .catch(nullValidationErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should not create attribute without value', () => sqlz.attribute.create({
            name: 'testAttrib',
            type: 'ATTR_NUMBER',
            categoryId: cat.dataValues.id
        })
            .catch(nullValidationErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should not create attribute duplicate', () => sqlz.attribute.create({
            name: 'testAttrib',
            type: 'ATTR_NUMBER',
            value: {},
            categoryId: cat.dataValues.id
        })
            .then(attribute => sqlz.attribute.create({
                name: 'testAttrib',
                type: 'ATTR_LIST',
                value: {},
                categoryId: attribute.dataValues.categoryId
            }))
            .catch(duplicateErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should reassign attribute (move)', () => {
            let src = null;
            return sqlz.attribute.create({
                name: 'testAttrib',
                type: 'ATTR_NUMBER',
                value: {},
                categoryId: cat.dataValues.id
            })
                .then((attr) => {
                    src = attr;
                    return attr;
                })
                .then(() => sqlz.categories.create({
                    name: 'test2'
                }))
                .then(category => sqlz.attribute.update({
                    categoryId: category.dataValues.id
                }, {
                    where: {
                        id: src.dataValues.id
                    }
                }));
        });
        it('should not reassign exist attribute (create duplicate)', () => {
            let src = null;
            return sqlz.attribute.create({
                name: 'testAttrib',
                type: 'ATTR_NUMBER',
                value: {},
                categoryId: cat.dataValues.id
            })
                .then((attr) => {
                    src = attr;
                    return attr;
                })
                .then(() => sqlz.categories.create({
                    name: 'test2'
                }))
                .then(category => sqlz.attribute.create({
                    name: 'testAttrib',
                    type: 'ATTR_NUMBER',
                    value: {},
                    categoryId: category.dataValues.id
                })
                    .then(() => category))
                .then(category => sqlz.attribute.update({
                    categoryId: category.dataValues.id
                }, {
                    where: {
                        id: src.dataValues.id
                    }
                }))
                .catch(duplicateErrorHandler);
        });
        it('should rename attribute', () => sqlz.attribute.create({
            name: 'testAttrib',
            type: 'ATTR_NUMBER',
            value: {},
            categoryId: cat.dataValues.id
        })
            .then(attr => sqlz.attribute.update({
                name: 'testAttrib2'
            }, {
                where: {
                    id: attr.dataValues.id
                }
            })));
        it('should not rename attribute to exist name (create duplicate)', () => sqlz.attribute.create({
            name: 'testAttrib',
            type: 'ATTR_NUMBER',
            value: {},
            categoryId: cat.dataValues.id
        })
            .then(() => sqlz.attribute.create({
                name: 'testAttrib2',
                type: 'ATTR_NUMBER',
                value: {},
                categoryId: cat.dataValues.id
            }))
            .then(attr => sqlz.attribute.update({
                name: 'testAttrib'
            }, {
                where: {
                    id: attr.dataValues.id
                }
            }))
            .catch(duplicateErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should change description of attribute', () => sqlz.attribute.create({
            name: 'testAttrib',
            type: 'ATTR_NUMBER',
            value: {},
            categoryId: cat.dataValues.id
        })
            .then(attr => sqlz.attribute.update({
                description: 'Some test description'
            }, {
                where: {
                    id: attr.dataValues.id
                }
            })));
        it('should change value of attribute', () => sqlz.attribute.create({
            name: 'testAttrib',
            type: 'ATTR_NUMBER',
            value: {},
            categoryId: cat.dataValues.id
        })
            .then(attr => sqlz.attribute.update({
                value: {
                    min: 0,
                    max: 10
                }
            }, {
                where: {
                    id: attr.dataValues.id
                }
            })));
        it('should delete attribute', () => sqlz.attribute.create({
            name: 'testAttrib',
            type: 'ATTR_NUMBER',
            value: {},
            categoryId: cat.dataValues.id
        })
            .then(attr => sqlz.attribute.destroy({
                where: {
                    id: attr.dataValues.id
                }
            })));
    });
    describe('Products', () => {
        beforeEach(() => sqlz.sequelize
            .sync({
                force: true
            }));
        it('should create product', () => sqlz.product.create({
            name: 'product',
            description: '',
            attribs: {}
        }));
        it('should not create product duplicate', () => sqlz.product.create({
            name: 'product',
            description: '',
            attribs: {}
        })
            .then(() => sqlz.product.create({
                name: 'product',
                description: '',
                attribs: {}
            }))
            .catch(duplicateErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should not create product without description', () => sqlz.product.create({
            name: 'product',
            attribs: {}
        })
            .catch(nullValidationErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should not create product without attributes', () => sqlz.product.create({
            name: 'product',
            description: ''
        })
            .catch(nullValidationErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should rename product', () => sqlz.product.create({
            name: 'product',
            description: '',
            attribs: {}
        })
            .then(product => sqlz.product.update({
                name: 'other_product'
            }, {
                where: {
                    id: product.dataValues.id
                }
            })));
        it('should not rename product to exist name (create duplicate)', () => sqlz.product.create({
            name: 'product',
            description: '',
            attribs: {}
        })
            .then(() => sqlz.product.create({
                name: 'other_product',
                description: '',
                attribs: {}
            }))
            .then(product => sqlz.product.update({
                name: 'product'
            }, {
                where: {
                    id: product.dataValues.id
                }
            }))
            .catch(duplicateErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should change product description', () => sqlz.product.create({
            name: 'product',
            description: '',
            attribs: {}
        })
            .then(product => sqlz.product.update({
                description: 'updated description'
            }, {
                where: {
                    id: product.dataValues.id
                }
            })));
        it('should change product attributes', () => sqlz.product.create({
            name: 'product',
            description: '',
            attribs: {}
        })
            .then(product => sqlz.product.update({
                attribs: {
                    'some attrib': true
                }
            }, {
                where: {
                    id: product.dataValues.id
                }
            })));
    });
    describe('Variants', () => {
        let product = null;
        beforeEach(() => sqlz.sequelize
            .sync({
                force: true
            })
            .then(() => sqlz.product.create({
                name: 'product',
                description: '',
                attribs: {}
            }))
            .then((p) => {
                product = p;
                return p;
            }));
        it('should create variant of product', () => sqlz.variant.create({
            name: 'productVariant',
            description: '',
            attribs: {},
            productId: product.dataValues.id
        }));
        it('should not create unrelated variant', () => sqlz.variant.create({
            name: 'productVariant',
            description: '',
            attribs: {}
        })
            .catch(nullValidationErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should not create variant without description', () => sqlz.variant.create({
            name: 'productVariant',
            attribs: {},
            productId: product.dataValues.id
        })
            .catch(nullValidationErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should not create variant without name', () => sqlz.variant.create({
            description: '',
            attribs: {},
            productId: product.dataValues.id
        })
            .catch(nullValidationErrorHandler)
            .then(shouldNotBeSuccessful));
        it('should not create variant without attributes', () => sqlz.variant.create({
            name: 'productVariant',
            description: '',
            productId: product.dataValues.id
        })
            .catch(nullValidationErrorHandler)
            .then(shouldNotBeSuccessful));
    });
    describe('User', () => {
        it('should be tested');
    });
    describe('Role', () => {
        it('should be tested');
    });
    describe('Offer', () => {
        it('should be tested');
    });
    describe('Images', () => {
        it('should be tested');
    });
});
