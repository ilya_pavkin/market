const env = process.env.NODE_ENV || 'development';
const path = require('path');
const Spawn = require('child_process').spawn;

const cfg = require(path.join(__dirname, '..', 'config', 'dbconfig.json'));

const self = {
    dumpDatabase: (name, next, forceEnv) => {
        const config = (forceEnv !== undefined) ? cfg[forceEnv] : cfg[env];
        const pgDump = Spawn('pg_dump', [`--file=${name}.sql`, `--dbname=postgresql://${config.username}:${config.password}@${config.host}:5432/${config.database}`]);
        pgDump.on('exit', (code) => {
            console.log(`[Dump database] pg_dump exited with code ${code.toString()}`);
            if (next !== undefined) {
                next();
            }
        });
    },

    dumpSchema: (name, next, forceEnv) => {
        const config = (forceEnv !== undefined) ? cfg[forceEnv] : cfg[env];
        const pgDump = Spawn('pg_dump', ['--schema-only', `--file=${name}.sql`, `--dbname=postgresql://${config.username}:${config.password}@${config.host}:5432/${config.database}`]);
        pgDump.on('exit', (code) => {
            console.log(`[Dump schema] pg_dump exited with code ${code.toString()}`);
            if (next !== undefined) {
                next();
            }
        });
    },

    restoreDatabase: (name, next, forceEnv) => {
        const config = (forceEnv !== undefined) ? cfg[forceEnv] : cfg[env];
        const psql = Spawn('psql', [`--dbname=postgresql://${config.username}:${config.password}@${config.host}:5432/${config.database}`, `--file=${name}.sql`]);
        psql.on('exit', (code) => {
            console.log(`[Restore database] psql (${name}.sql) exited with code ${code.toString()}`);
            if (next !== undefined) {
                next();
            }
        });
    },

    execSql: (cmd, next, forceEnv) => {
        const config = (forceEnv !== undefined) ? cfg[forceEnv] : cfg[env];
        const psql = Spawn('psql', [`--dbname=postgresql://${config.username}:${config.password}@${config.host}:5432`, `-c ${cmd}`]);
        psql.on('exit', (code) => {
            console.log(`[Exec] psql (${cmd}) exited with code ${code.toString()}`);
            if (next !== undefined) {
                next();
            }
        });
    },

    createDatabase: (next, forceEnv) => {
        const config = (forceEnv !== undefined) ? cfg[forceEnv] : cfg[env];
        self.execSql(`CREATE DATABASE ${config.database}\n` +
            '    WITH \n' +
            '    OWNER = postgres\n' +
            '    ENCODING = \'UTF8\'\n' +
            '    CONNECTION LIMIT = -1;', next, forceEnv);
    },

    dropDatabase: (next, forceEnv) => {
        const config = (forceEnv !== undefined) ? cfg[forceEnv] : cfg[env];
        self.execSql(`drop database ${config.database}`, next, forceEnv);
    }
};

module.exports = self;
