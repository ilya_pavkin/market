const SequelizeAuto = require('sequelize-auto');
const DBUtil = require('../util/database-test-asset.js');

const self = {
    loadDatabase: (name, user, password) => {
        const auto = new SequelizeAuto(name, user, password, {
            host: '127.0.0.1',
            dialect: 'postgres',
            directory: './db',
            port: '5432',
            additional: {
                timestamps: false
            },
            indentation: 4
        });
        auto.run((err) => {
            if (err) {
                throw err;
            }
            //console.log(auto.tables);
            //console.log(auto.foreignKeys);
        });
    }
};

self.loadDatabase('market', 'postgres', '123');

DBUtil.dumpDatabase('.tmp/last-backup');

module.exports = self;
