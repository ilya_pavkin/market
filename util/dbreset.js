const DBUtil = require('../util/database-test-asset.js');

DBUtil.dropDatabase(() => {
    DBUtil.createDatabase(() => {
        DBUtil.restoreDatabase('.tmp/last-backup');
    });
});