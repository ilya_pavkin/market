const entities = require('../lib/database.js');

class Sample {
    static run(cb) {
        console.log('run');
        entities.sequelize.sync()
            .then(() => {
                entities.categories.create({
                    name: 'test',
                    description: 'Desc'
                })
                    .then((created) => {
                        console.log('ok!');
                        cb(null, created);
                    })
                    .catch((err) => {
                        console.log('error!');
                        cb(err);
                    });
            })
            .catch((err) => {
                console.error(err);
            });
    }
}

module.exports = Sample;
